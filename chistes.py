#!/usr/bin/env python3
# CHISTES CORTOS
""" Candidatos para tomar el chiste:
            http://www.chistes.com/chistealazar.asp?n=3
            https://www.chister.com/azar.php

        Fuente para capturar cuando se cita en Mastodon copiado de:
            https://www.bortzmeyer.org/fediverse-bot.html
            https://framagit.org/bortzmeyer/mastodon-DNS-bot
"""

# CONSTANTES
MI_NOMBRE = "chistes"
TIEMPO_ESPERA = 9
URL_CHISTES = 'https://www.chister.com/azar.php'
INTENTOS = 3 # Numero de intentos para conseguir chiste; si no se localiza no se publica nada
MASTODON_MAX = 500
MISTETAS = "Una señora tiene un perro llamado Mistetas. Un día, mientras lo pasea, se le escapa.\nLa señora busca a un guardia y le dice: ¿Perdone, agente, ha visto usted a Mistetas?\nA lo que éste contesta: NO, PERO ME GUSTARÍA VERLAS"
VANDOS = "Van dos y se cae el del medio"

# LIBRERIAS
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from dateutil import tz
from lxml import html
from mastodon import Mastodon, StreamListener
import configparser
import requests
import time
import sys
import re

# FUNCIONES
def args():
    #Argumentos
    parser = ArgumentParser( description='Extrae un chiste de la web' )
    parser.add_argument( '-p', '--pantalla' , help='Muestra el chiste en pantalla', action="store_true", default=False )
    parser.add_argument( '-m', '--mastodon', help='Publica el chiste en Mastodon', action="store_true", default=False )
    parser.add_argument( '-e', '--escuchar', help='Se queda esperando a ser citado para responder', action="store_true", default=False )
    parser.add_argument( '-t', '--tiempo', help='Publicar un chiste cada TIEMPO en minutos', type = int )
    parser.add_argument( '-c', '--configura', dest = 'fich_config', help = 'Fichero del que tomar los datos de conexión', required = False )
    # El Required de fich_config debería ser True pero, por seguridad, voy a pasar los datos mediantes variables de entorno
    parser.add_argument( '-i', '--inst_url', help='URL de la instancia a conectar', type=str )
    parser.add_argument( '-a', '--app_token', help='Token de la app registrada en la cuenta a usar', type=str )
    return parser.parse_args()

def datos_conex( arg_nom_fich, arg_inst_url, arg_app_token  ):
    # Leer datos de conexion a Mastodon
    inst_url=""
    app_token=""
    if arg_inst_url:
        inst_url = arg_inst_url
    if arg_app_token:
        app_token = arg_app_token
    if arg_nom_fich:
        config = configparser.ConfigParser()
        config.read( arg_nom_fich )
        inst_url = config['DEFAULT']['INST_URL']
        app_token = config['DEFAULT']['APP_TOKEN']
    if not ( inst_url or app_token ):
        print( "Faltan los datos de conexión" )
        sys.exit()
    mastodon = Mastodon( access_token = app_token, api_base_url = inst_url )
    return mastodon

def abrir_pagina( la_URL ):
    # Abrir página y meter en un objeto
    pagina_web = requests.get( la_URL )
    bsObj = BeautifulSoup( pagina_web.content, 'lxml' )
    return bsObj

def  buscar_pagina( bsObj ):
    # Buscar chiste en la  pagina
    tabla = bsObj.find( 'table', attrs={'width': 607})
    fila = ""
    contador = 1
    chiste = ""
    for fila in tabla:
        contador += 1
        if( contador == 5 ):
            chiste = fila.get_text()
            chiste = chiste.strip()
            indice = len( chiste )
            while indice>0 and ord( chiste[ indice-1:indice ] ) != 9: indice -= 1
            while indice>0 and ord( chiste[ indice-1:indice ] ) == 9: indice -= 1
            chiste = chiste[ 0:indice ]
            chiste = chiste.replace( "\r", "\n" )
    return chiste

def formatear_chiste( chiste, cabecera ):
    # Generar la cadena completa a imprimir
    if len( chiste ) == 0:
        chiste = MISTETAS
    chiste = cabecera + "\n---\n" + str( chiste )
    return chiste

def buscar_chiste( mi_URL, cabecera ):
    # Buscar chiste
    contador = INTENTOS
    chiste = ""
    while contador > 0 and len( chiste )<1:
        time.sleep( TIEMPO_ESPERA )
        chiste = buscar_pagina( abrir_pagina( mi_URL ) )
        chiste = formatear_chiste( chiste, cabecera )
    return chiste

def tootear( mastodon, chiste, en_respuesta_a=0, visibilidad='public' ):
    # Opciones de visibilidad: direct, private, unlisted, public
    if en_respuesta_a:
        mastodon.status_post( chiste, in_reply_to_id = en_respuesta_a, visibility = visibilidad )
    else:
        mastodon.status_post( chiste, visibility = visibilidad )

def chistaco():
    # https://sodocumentation.net/es/python/topic/484/fecha-y-hora
    thora_local = tz.gettz() # Local time
    thora = tz.gettz( 'Europe/Madrid' )
    hora_local = datetime.now()
    hora = hora_local.astimezone( thora )
    salida = buscar_chiste( URL_CHISTES, "Chistaco de las " + hora.strftime("%X") )
    return salida

def poner_citados( mi_nombre, lista_menciones, cadena ):
    cad_final = ''
    for elemento in lista_menciones:
        if elemento['acct'] != mi_nombre:
            cad_final = cad_final + ' @' + elemento['acct']
    cad_final = cad_final.strip()
    if cad_final != '':
        cadena = cadena + '\n' + cad_final
    return cadena

def ayuda( mastodon, solicitante, en_respuesta_a, visibilidad ):
    mensaje = "@" + solicitante +'\n'
    mensaje = mensaje + 'Bot de proceso de mensajes'
    mensaje = mensaje + '\n---\n'
    mensaje = mensaje + 'El bot procesa un comando dentro del mensaje con las siguientes opciones:\n'
    mensaje = mensaje + '<h> Esta ayuda\n'
    mensaje = mensaje + '<r> Devolver un chiste\n'
    mensaje = mensaje + '\nNo se procesan solicitudes de bots'
    tootear( mastodon, mensaje, en_respuesta_a, visibilidad )

def run_chiste( mastodon, id_mensaje, solicitante, cuerpo, visibilidad, mi_nombre, lista_menciones ):
    respuesta = "@" + solicitante
    respuesta = buscar_chiste( URL_CHISTES, respuesta )
    if len( respuesta ) >= MASTODON_MAX:
            #respuesta = "Ha habido un error @" + solicitante + "\nPor favor, vuelve a intentarlo."
            respuesta = VANDOS
    respuesta = poner_citados( mi_nombre, lista_menciones, respuesta )
    tootear( mastodon, respuesta, id_mensaje, visibilidad )

def procesar( mastodon, id_mensaje, solicitante, cuerpo, visibilidad, mi_nombre, lista_menciones ):
    # Comandos: h, r
    minus = cuerpo.lower()
    if minus.find( '<h>' )>0:
        ayuda( mastodon, solicitante, id_mensaje, visibilidad )
    elif minus.find( '<r>' )>0:
        run_chiste( mastodon, id_mensaje, solicitante, cuerpo, visibilidad, mi_nombre, lista_menciones )
    return

class mi_escuchador( StreamListener ):
# instance of the class StreamListener from Mastodon.py, and to provide routines to act when a given event takes place.
    def __init__( self, mastodon ):
        self.masto = mastodon

    def on_notification(self, notificacion):
        try:
            qname = None
            qtype = None
            solicitante = None
            visibilidad = None
            if notificacion['type'] == 'mention':
                id_mensaje = notificacion['status']['id']
                solicitante = notificacion['account']['acct']
                encontrado = re.match( "^.*@(.*)$", solicitante )
                visibilidad = notificacion['status']['visibility']
                # Mastodon API returns the content of the toot in
                # HTML, just to make our lifes miserable
                doc = html.document_fromstring( notificacion['status']['content'] )
                # Preserve end-of-lines
                # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
                for br in doc.xpath("*//br"):
                            br.tail = "\n" + br.tail if br.tail else "\n"
                for p in doc.xpath("*//p"):
                            p.tail = "\n" + p.tail if p.tail else "\n"
                body = doc.text_content()
                # No contestar a bots
                if notificacion['account']['bot'] == False:
                    procesar( self.masto, id_mensaje, solicitante, body, visibilidad, self.masto.account_verify_credentials()['username'], notificacion['status']['mentions'] )
        except KeyError as error:
            self.log.error( "Malformed notification, missing %s" % error )
        except Exception as error: 
            self.log.error( "%s %s/%s -> %s" % (sender, qname, qtype, error) )

# MAIN
argumentos = args()
mastodon = datos_conex( argumentos.fich_config, argumentos.inst_url, argumentos.app_token )
if argumentos.mastodon:
    tootear( mastodon, chistaco() )
elif argumentos.tiempo:
    # Parte del bot para chiste cada hora
    # Gestión de tareas de # https://m.forocoches.com/foro/showthread.php?t=7112425
    minutos = timedelta( minutes = argumentos.tiempo )
    thora_local = tz.gettz()
    thora = tz.gettz( 'Europe/Madrid' )
    hora_local = datetime.now()
    hora = hora_local.astimezone( thora )
    alarma = hora
    # alarma = alarma.replace( hour = 10, minute = 47, second = 0, microsecond = 0 )
    while True:
        hora_local = datetime.now()
        hora = hora_local.astimezone( thora )
        if hora > alarma:
            tootear( mastodon, chistaco() )
            alarma = alarma + minutos
        time.sleep( TIEMPO_ESPERA )

elif argumentos.escuchar:
    # Parte del bot para responder
    while True:
        escuchar = mi_escuchador( mastodon )
        mastodon.stream_user( escuchar )
        time.sleep( TIEMPO_ESPERA )
else:
    print ( chistaco() )
